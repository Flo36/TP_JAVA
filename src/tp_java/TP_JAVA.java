/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_java;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *
 * @author frichard
 */
public class TP_JAVA {

    private static final Logger LOG = Logger.getLogger(TP_JAVA.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * Exercice 0
         */
        LOG.log(Level.INFO, args[0] + " " + args[1] + " " + args[2] + " " + args[3] + " " + args[4] + " " + args[5]);
        //Exercice terminé

        /*
     *Exercice 1
     * Appel de la méthode Facto
         */
        Maths.facto(4);
        /*
    Fin Exercice 1
         */
        Maths.askFacto();

        /*
    Fin Exercice 2
         */
        int[] tableau = {1, 2, 3, 54, 69, 72};
        /* ArrayList<Integer> newTableau =  */
        Maths.tableauPair(tableau);
    }

}
