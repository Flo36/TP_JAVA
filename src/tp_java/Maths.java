/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_java;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author frichard
 */
public class Maths {

    public static void facto(int nb) {
        int nbFacto = 1;
        for (int i = 1; i <= nb; i++) {
            nbFacto = (i) * (nbFacto);
        }
        System.out.println("La Factorielle de " + nb + " est " + nbFacto + ".");
    }

    /*
    Fin Exercice 1, voir TP_JAVA.java
     */

 /*
    Exercice 2 :
     */
    public static void askFacto() {
        Scanner sc = new Scanner(System.in);
        String str = "";
        do {
            System.out.println("Veuillez saisir une valeur : ");
            if (sc.hasNext()) {
                str = sc.nextLine();
                try {
                    int nb = Integer.parseInt(str);
                    facto(nb);
                } catch (NumberFormatException e) {

                }
            }
        } while (!"Q".equals(str.toUpperCase()));

    }

    /*
Fin Exercice 2 terminé, voir TP_JAVA.java
     */

 /*
Exercice 3
     */
    public static void tableauPair(int[] tableau) {
        ArrayList<Integer> newTableau = new ArrayList<>();
        
        for (int i = 0; i < tableau.length; i++) {
            int v = tableau[i];
            if (v % 2 == 0) {
                System.out.println(v);
            }

        }
    }
}
